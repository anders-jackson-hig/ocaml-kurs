(* label argument och 'a option och dess map
   
   val: map ('a -> 'b option) 'a option -> 'b option

   kommer i modulen Option till Ocaml 4.08.0 *)

(*
 För mer om Label-argument se:
 https://caml.inria.fr/pub/docs/manual-ocaml/lablexamples.html
 *) 

(* vanlig fold utan label-argument *)
let rec fold func acc lst =
  match lst with
  | [] -> acc
  | a :: rest -> fold func (func a acc) rest

(* det måste finnas minst ett icke-option argument *)
(* om alla är optional, sätt dit ett unit. *)
let rec fold_label ?(f=(fun a b -> a + b)) ?(acc=0) ?(lst=[]) () =
  match lst with
  | [] -> acc
  | a :: rest -> fold_label ~f:f ~acc:(f a acc) ~lst:rest ()

let _ =
  let a = fold (fun a b -> a + b) 0 [1; 2; 3] in
  let b = fold_label () in
  Printf.printf "\nSkriv ut fold och fold_label\n";
  Printf.printf "fold       : %d\n" a;
  Printf.printf "fold_label : %d\n" b

(* Järnvägsmönstret *)
(* Man skriver funktioner som gör deljob av problemet man 
 * vill lösa. Så låter man dem returnera Some x om det
 * gick utan problem, och None om det gick dåligt.
 * Då kan man med map-funktionen på 'a option då "kortsluta"
 * resterande funktionsanrop och låta allt returnera None
 *
 * Ett vanligt mönster att programmera funktionellt på.
 * Namnet eftersom man "rangerar" ut None från efterföljande
 * beräkningar. Dvs växlar förbi alla beräkningar
 *)

let option_int_to_string = function
  | Some a -> "Some " ^ string_of_int a
  | None -> "None"

(*
 * Returnerar värdet av funktionen (func v) value är Some v,
 * om value är None, så returnera None.
 *)
(* val map : ('a -> 'b option) -> 'a option -> 'b option *)
let map func value = 
  match value with
  | Some a -> func a
  | None -> None
exception No_value
(* Om v är Some value, returnera value annars generera exception *)
(* val return : 'a option -> 'a *)
let return v =
  match v with
  | None -> raise No_value
  | Some a -> a
let get v = return

let a = None
let b = Some 1

let () =
  let a = map (fun a -> Some (10 + a)) None in
  let b = map (fun a -> Some (10 + a)) (Some 10) in
  Printf.printf "\nfun = (fun a -> Some (10 + a))\n";
  Printf.printf "map fun None => %s\n" (option_int_to_string a);
  Printf.printf "map fun (Some 10) => %s\n" (option_int_to_string b)


(*
 * Här skriver vi ett program som kör funktionerna
 * funcA och funcB (och funcC) efter varandra.
 * Om något steg misslyckas, så misslyckas hela
 * programmet.
 * Notera att funktionerna bara behöver hantera sitt
 * egna arbete. De behöver inte ta hänsyn till om
 * föregående steg misslyckats.
 *)

let () =
(* Definiera några funktioner, funcC misslyckas alltid *)
  let funcA = fun a -> Some (10 + a) in
  let funcB = fun a -> Some (a - 12) in
  let funcC = fun a -> None in
(* Sedan anropar vi funcA b och sedan funcB på returvärdet *)
  let aval = map funcB (map funcA b) in (* Some -1 *)
(* kan skrivas så här: Notera: indata först, sedan funktionerna *)
  let bval = b |> map funcA |> map funcB in (* Some -1 *)
  let cval = a |> map funcA |> map funcB in (* indata misslyckat *)
  let dval = b |> map funcA |> map funcB |> map funcC in (* funcC misslyckas *)
  let eval = b |> map funcA |> map funcC |> map funcB in (* funcC misslyckas *)
  let fval = b |> map funcA |> map funcA |> map funcB in (* Some 9 *)
  (* Skriv ut värdena som beräknats *)
  Printf.printf "\nSkriver ut beräkningarna som gjorts\n";
  Printf.printf "aval = %s\n" (option_int_to_string aval);
  Printf.printf "bval = %s\n" (option_int_to_string bval);
  Printf.printf "cval = %s\n" (option_int_to_string cval);
  Printf.printf "dval = %s\n" (option_int_to_string dval);
  Printf.printf "eval = %s\n" (option_int_to_string eval);
  Printf.printf "fval = %s\n" (option_int_to_string fval)

(*
 * Kan skrivas utan 'a option, men det blir krångligt och med
 * många if eller match-satser
 * Så här med if-else:

if (b = None) then None
else let valb = funcA (return b) in
if (valb = None) then None
else let valc = funcB (return valb) in
print_int_option valc

 * Jämfört med:

print_int_option (b |> map funcA |> map funcB)
 *)
