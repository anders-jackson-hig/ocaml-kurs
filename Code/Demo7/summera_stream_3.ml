(**
 * Programmet läser in flyttal från en fil
 * och skriver ut summera eller subtreahera alla talen,
 * beroende på om switchen -a, --add, -s, --sub anges till programmet.
 *
 * Filnamn skall anges på kommandoraden.
 *
 * Använd Arg-modulen.
 *
 * Börja med att läsa från en fil, ex "data.txt".
 * Skriv bara ut varje rad.
 *
 * Kompilera:     C-c C-c  : "ocamlbuild -cflags -annot summera_stream_3.byte"
 * Leta fel:      C-c C-x eller [f3] eller [S-f3]
 * Exekvera:      M-x shell: "./summera_stream_3.byte"
 * Testa i REPL:  Tuareg -> Interactive mode -> Run OCaml REPL
 *)

(** Standardvärden om addering eller subtrathering *)
let add = ref true
(** Standardvärde på vilken fil som skall adderas/subtraheras *)
let filnamn = ref "data.txt"

let processa_rad op tal acc =
  op acc tal

let rec processa_rader stream op acc =
  match Stream.peek stream with
  | None -> acc
  | Some _ ->
     (processa_rad op (Stream.next stream) acc) |> processa_rader stream op
(*   prosessa_rader op stream (processa_rad (Stream.next stream) op acc) *)

 let main filnamn =
  let infil = open_in filnamn in
  let callback infil _ = try
      Some (input_line infil |> String.trim |> float_of_string)
    with
    | End_of_file -> None
    | Failure str -> Some 0.0 in
  let stom = Stream.from (callback infil) in
  try
    if !add then
      begin
        print_float (processa_rader stom ( +. ) 0. );
        print_newline ()
      end
    else
      begin
        print_float (processa_rader stom ( -. ) (Stream.next stom));
        print_newline ()
      end;
    close_in infil
  with e ->
    close_in infil;
    raise e


 
 (* Processa argumenten *)
 let () =
   let spec = [("-a", Arg.Set add, "\t eller");
               ("--add", Arg.Set add, "\t Addera talen");
               ("-s", Arg.Clear add, "\t eller");
               ("-sub", Arg.Clear add, "\t Subtrahera talen")]
   in
   Arg.parse
     (Arg.align spec)
     (fun str -> filnamn := str)
     "Addera eller subtreahera talen i en angiven fil\n"
 
 (* Kör programmet *)
 let () =
   main !filnamn

