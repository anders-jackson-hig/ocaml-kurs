
(* Läs argument från kommandoraden.
 *
 * https://ocaml.org/learn/tutorials/command-line_arguments.html
 *
 * Vi gör det enkelt för oss och använder oss av 
 * variabeln sys i modulen 
 * Sys.argv : string array
 * 
 * Programmet gör om den till en lista, och sedan skriver
 * ut listan.
 *
 * ocamlbuild -cflags -annot get_arguments.byte
 * 
 * $ ./get_arguments.byte a -b c
 * Argument 0: ./get_arguments.byte
 * Argument 1: a
 * Argument 2: -b
 * Argument 3: c
 * ./get_arguments.byte
 * a
 * -b
 * c
 * $
 *
 * 20190527 Anders Jackson
 *)

let () =
  (* Ta argumenten och gör om dem till en lista *)
  let arguments = Array.to_list Sys.argv in
  let rec print_args nr =
    function
    | [] -> ()  (* tom list, så vi är färdig *)
    | arg :: rest ->
      Printf.printf "Argument %d: %s\n" nr arg ;
      print_args (nr+1) rest
  in
  print_args 0 arguments

let () =
  (* Eller så använder vi färdiga rutiner *)
  print_newline ();
  List.iter print_endline (Array.to_list Sys.argv)

(* eof *)
    
    
