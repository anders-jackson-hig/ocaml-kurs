(* Skiss på lösning av att hitta stig eller väg i en graf
 *
 * Löst baserat på hur Djiikstras algoritm för att hitta 
 * kortaste stigen i en graf fungerar.
 * 
 * 20190527 Anders Jackson
*)

(* Ni skall skriva en funktion av detta. Helst rekursiv
 *
 * Graph.t är den graf ni använder
 * (Path.t -> bool) är en funktion som är true om Path.t är en stig/väg
 * Path.t list är en lista med alla stigar/vägar i graf
 *
 * val get_all_paths : Graph.t -> (Path.t -> bool) -> Path.t list *)
let get_all_paths check_path graf =
  let queue = List.map Path.make_path (Graph.get_corners graf) in
  (* Detta skall iterera tills att queue har slut på stigar *)
  let path = List.hd queue in                (* gör en pop ut kön *)
  let queue = List.tl queue in
  let acc_path = path :: acc_path in         (* spar pathen, i stigande längd *)
  let paths = Path.expand_path graph path in (* expandera path:en till lista av pather *)
  let (okpaths, nopaths) = List.partition check_path paths in (* fliltrera de korrekta path:erna *)
  let queue = queue @ okpaths in             (* lägg till de korrekta path:erna i slutet av kön *)
  (* Repetera tills slut i kön *)
  (* Här har vi alla stigar *)
  acc_path

