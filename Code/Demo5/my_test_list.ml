#!/usr/bin/env ocaml
;;
(** my_test_list.ml

   Modulen [List] innehåller många funktioner som är användbara när
   man hanterar listor.

   Funktionen [List.find] tar en funktion och returnerar ett element
   som den ger [true] för.  Den kastar ett [exception Not_found] om
   den inte hittar något.  Exception är inte normalfall, och att inte
   hitta är normlt.

   Funktionen [List.fin_opt] fungerar som [List.find], men returnerar
   en ['a option] istf ['a]. Och behöver då inte kasta ett exception
   om den inte hittar.  Hittar dem data. så returnerar den [Some 'a],
   och finns inte data så returnerar den [None].

   Många funktioner finns i varianten som returnerar ['a] eller
   kastar ett [Not_found] eller liknande vid fel.  De finns även i
   varianten med [_opt] i slutet av namnet, och returnerar ['a option]
   istället. Dvs [None] om inget hittat och [Some v] om hittat.  Då
   behöver man inte hantera exception.  Hurra!  *)

(* Läses in som modulen My_test_list i utop med 
   #mod_use "my_test_list.ml";; *)

(*
(* Använd helst inte, man får in ALLA namnen i modulen. *)
open List
 *)

let temp = [1; 2; 3]
let temp2 = List.exists (fun a -> a = 2) temp
(* temp3 är en sk associationslista, med nyckel*data *)
let temp3 = [("a", 12);("b", -3); "c",42;]
;;
(* Main-funktionen som senare skall startas *)
let main _ =
  (* Här öppnar vi modulen List bara för detta (långa) uttryck i in-delen.
     på så vis slipper vi skiva List. på många ställen *)
  let open List in
  print_newline (); print_int (length temp); print_newline ();
  (* print_string (" temp2 = "^(string_of_bool temp2)); print_newline (); *)
  Printf.printf " temp2 = %b\n" temp2;
  (* print_int (List.assoc "b" temp3); print_newline (); *)
  Printf.printf " List.assoc \"b\" temp3 = %d\n" (List.assoc "b" temp3);
  (* Här öppnar vi listan bara för uttrycket inom parenteserna *)
  Printf.printf " List.mem_assoc \"d\" temp3 = %b\n" (List.mem_assoc "d" temp3);
  Printf.printf " List.find temp = %d\n" (List.find (fun x -> x > 1) temp);
  Printf.printf " List.find_opt temp = %s\n" (match List.find_opt (fun x -> x > 1) temp with
                                              | None -> "None"
                                              | Some v -> "Some "^(string_of_int v))
;;

(* Stara programmet *)

let () =
  main ()
;;
