#!/usr/bin/env ocaml
;;
(* Detta gör att filen kan köras som program i linux om x-rättighet
   satt. Dock går det inte att kompilera den.

   Men det är ju redan avsett att läsas in i utop(1), eftersom det
   finns ett antal #-direktiv. *)

(** my_test_array.ml

   Testar modulen Array.

   Den har två funktioner för att skapa en array, [Array.init] och
   [Array.make].

   [Array.make] skapar en array av en angiven storlek, och initierar
   den med det värde som anges.

   [Array.init] skapar en array av angiven storlek, och initierar den
   genom att anropa en funktion som tar positionen och returnerar
   värdet.

   Funktionen [Printf.printf] i modulen [Printf] är användbar för
   formaterad utskrift.  *)

(* Läses in med #use "my_test_array.ml";; *)

let my_array = Array.init 10 (fun pos -> 2*pos)
;;
let my_small_array = Array.make 3 "2"
;;
let my_mapped_array = Array.map (fun pos -> 5*pos) my_array
;;
#show my_array;;
#show my_small_array;;
#show my_mapped_array;;

let main _ =
  print_newline ();
  print_endline (string_of_int my_array.(3));
  Printf.printf " Array.length my_array = %d\n" (Array.length my_array);
  Array.iter (fun v -> print_string ((string_of_int v)^", ")) my_array;
  print_newline ();
  Array.iter (fun v -> print_int v; print_string ", ") my_mapped_array;
  print_newline ();
  Array.iter (fun v -> print_string (v^", ")) my_small_array;
  print_newline ()
;;
let () =
  main ()
;;
