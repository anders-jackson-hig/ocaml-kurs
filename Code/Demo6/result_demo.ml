(* fil: result_demo.ml *)

(*
   Demo om Result eller Either som det även kallas.contents

   Datum    Vem               Varför
   -------- ----------------- ------------------
   20200512 Anders Jackson    Första exemplet

*)

(** Demonstration av [('a,'b) result], även känt som [('a,'b) either]

   Alternativa implementationer av [('a, 'e) result] ser ut så här,
   det är även så det ser ut i många andra funktionella språk, och som
   det är känt i andra sammanhang än OCaml

[type ('a,'b) either =
   | Right of 'b
   | Left of 'a
]

    I OCaml har man standarddefinitionen, se modulen [Stdlib.Result], där
    det finns ett antal hjälpfunktoiner.

[type ('a,'b) result =
   | Ok of 'a
   | Error of 'b
]

   [('a, 'e) result] ser ut och behandlas på liknande sätt som ['a option],
   med skillnaden att med [('a*'e) result] även hanterar ev felvärden/meddelanden *)

(* div från option_demo.ml, om skriven med result *)

(* val div: (int*string) result -> (int*string) result -> (int*string) result *)
let div x y =
  match x with
  | Error e -> Error e
  | Ok x' ->
    match y with
    | Error e -> Error e
    | Ok 0 -> Error "Division med noll"
    | Ok y' -> Ok (x' / y')

(* Som [div], men med [Result.bind] för att binda värdet hos [Ok v] till en variabel *)
let div' x y =
  Result.(bind x (fun x ->
          bind y (fun y ->
             if y = 0 then Error "div med noll!" else Ok (x / y))))

(* Funktioner kan ha sk label-argument, vilket kan sättas i vilken
   ordning som helt, bara man sätter [~namn:] för värdet.  Det är vad
   [Result.value v ~default:] har.  Se dokumentationen för
   [Result.value] 

   Datatyp: [val value: ('a, 'e) result -> default:'a -> 'a] *)

let maina () =
  let open Result in
  let a = ok 10 in
  let b = ok 2 in
  let c = Result.error "Fel" in
  let open Printf in
  printf "div ok 10 ok 2         = %d\n"   (Result.value (div' a b) ~default:(-1000));
  printf "div ok 10 error 'Fel'  = %d\n"   (Result.value (div' a c) ~default:(-1000));
  printf "div ok 10 error 'Fel'  = '%s'\n" (Result.get_error (div' a c));
  printf "div' ok 10 ok 2        = %d\n"   (Result.value ~default:(-1000) (div' a b));
  printf "div' ok 10 error 'Fel' = %d\n"   (Result.value (div' a c) ~default:(-1000));
  printf "div' ok 10 error 'Fel' = '%s'\n" (Result.get_error (div' a c))

let mainb () =
  let a = Result.ok 2 in
  let b = Result.error "Fel i b" in
  let funA a = Result.ok (a + 12) in
  let funB a = Result.ok (a - 10) in
  let funC a = Result.error "Fel i C" in
  let mybind f a = Result.bind a f in (* Result.bind har värde och sedan funktion *)
  let aval = Result.(a |> mybind funA |> mybind funB) in
  let bval = Result.(b |> mybind funA |> mybind funB) in
  let cval = Result.(b |> mybind funA |> mybind funB |> mybind funC) in
  let dval = Result.(a |> mybind funA |> mybind funB |> mybind funC) in
  let eval = Result.(a |> mybind funA |> mybind funC |> mybind funB) in
  let fval = Result.(a |> mybind funC |> mybind funB |> mybind funB) in
  let gval = Result.(a |> mybind funA |> mybind funB |> mybind funB) in
  let get_result a =
    if Result.is_ok a then "Value: "^(string_of_int (Result.get_ok a))
    else "Error: "^(Result.get_error a) in
  Printf.printf "Result: aval = %s\n" (string_of_int (Result.value ~default:0 aval));
  Printf.printf "Result: aval = %s\n" (get_result aval);
  Printf.printf "Result: bval = %s\n" (get_result bval);
  Printf.printf "Result: cval = %s\n" (get_result cval);
  Printf.printf "Result: dval = %s\n" (get_result dval);
  Printf.printf "Result: eval = %s\n" (get_result eval);
  Printf.printf "Result: fval = %s\n" (get_result fval);
  Printf.printf "Result: gval = %s\n" (get_result gval)
;;

(* Som i mainb, men vi definierar infix [( >>= )} som [Result.bind] för enklare syntax *)
let mainc () =
  let a = Result.ok 2 in
  let b = Result.error "Fel i b" in
  let funA a = Result.ok (a + 12) in
  let funB a = Result.ok (a - 10) in
  let funC a = Result.error "Fel i C" in
  let ( >>= ) = Result.bind in 
  let aval = a >>= funA >>= funB in
  let bval = b >>= funA >>= funB in
  let cval = b >>= funA >>= funB >>= funC in
  let dval = a >>= funA >>= funB >>= funC in
  let eval = a >>= funA >>= funC >>= funB in
  let fval = a >>= funC >>= funB >>= funB in
  let gval = a >>= funA >>= funB >>= funB in
  let get_result a =
    if Result.is_ok a then "Value: "^(string_of_int (Result.get_ok a))
    else "Error: "^(Result.get_error a) in
  Printf.printf "Result: aval = %s\n" (string_of_int (Result.value ~default:0 aval));
  Printf.printf "Result: aval = %s\n" (get_result aval);
  Printf.printf "Result: bval = %s\n" (get_result bval);
  Printf.printf "Result: cval = %s\n" (get_result cval);
  Printf.printf "Result: dval = %s\n" (get_result dval);
  Printf.printf "Result: eval = %s\n" (get_result eval);
  Printf.printf "Result: fval = %s\n" (get_result fval);
  Printf.printf "Result: gval = %s\n" (get_result gval)
;;

let () =
  print_endline "Main a";
  maina ();
  print_endline "Main b";
  mainb ();
  print_endline "Main c";
  mainc ();
  print_newline ()

(*
val parse_args : (arg_typ, string) result -> (data_typ, string) result

let main arg =
  let mybind f a = Result.bind a f in
  let res = arg |> mybind parse_args |> mybind read_data |> mybind calculate_result |> mybind generate_report in
  if Result.is_ok res then
    Printf.printf "Allt gick bra %s\n" (generate_ok_result Result.get_ok arg)
  else
    Printf.printf "Fel!!!! %s\n" (generate_error_message Result.get_error arg))
 *)
(* eof *)

