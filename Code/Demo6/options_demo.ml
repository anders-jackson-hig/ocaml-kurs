(* fil: options_demo.ml *)

(*
   Datum    Vem                  Vad
   -------- -------------------- ----------
   20200512 Anders Jackson       Första versionen
*)

(** Inbyggda datatypen ['a option] används för att hantera inget
   resultat, som i många andra programmeringsspråk hanteras med
   [NULL]. I OCaml så används istället vanligtvis datatypen ['a
   option].

   När en funktion får ett fel, eller inte kan beräkna ett resultat så
   returnerar den [None].  När den kan göra en korrekt beräkning, så
   returneras istället [Some v].

[type 'a option =
   | None
   | Some of 'a
]
*)

(* Kan köras från kommandoraden med `ocaml optiones_demo.ml' *)

let test_a arg =
  if arg < 0 then
    None
  else
    Some arg

(* val add: int -> int -> int option *)
(* val add: int option -> int option -> int option *)
let add x y =
  match x with
  | None -> None
  | Some a ->
    match y with
    | None -> None
    | Some b -> Some (a + b)
let sub x y =
  match x with
  | None -> None
  | Some x' ->
    match y with
    | None -> None
    | Some y' -> Some (x' - y')
let mul x y =
  match x with
  | None -> None
  | Some x' ->
    match y with
    | None -> None
    | Some y' -> Some (x' * y')
let div x y =
  match x with
  | None -> None
  | Some a ->
    match y with
    | None -> None
    | Some 0 -> None
    | Some b -> Some (a / b)

(* Långt och krångligt.  Kan det inte göras bättre?  Jo, med
   [mybind]. Den datat och en funktion som argument.  Funktionen
   anropas om det finns data, dvs om datat är [Some v] *)
let mybind v f =
  match v with
  | None -> None
  | Some v' -> f v'

(* Här använder vi [bind], för att göra 1:an tillgänglig som [x] i
   funktionen, som returnerar värdet + 1 *)
let a = mybind (Some 1) (fun x -> Some (x+1))

(* Här använder vi [bind] för att försöka binda argumentet [None] till
   [x]. Men det kommer inte att hända, eftersom [bind] ser att
   argumentet är [None], och därför körs inte funktionen. Så [bind]
   returnerar [None]. *)
let b = mybind None (fun x -> Some (x+1) ) 

(* Så här använder vi [bind] för att plocka ut argumenten och gör
   beräkningar.  Eftersom det existerar en [Option.bind],
   [Option.some] och [Option.none], så användes de istället. *)
let add' x y =
  Option.(bind x (fun x' ->                (* Bind x som x' och använd *)
      bind y (fun y' -> some (x' + y'))))  (* Bind y som y' och använd *)
let sub' x y =
  Option.(bind x (fun x' ->
      bind y (fun y' -> some (x' - y'))))
let mul' x y =
  Option.(bind x (fun x' ->
      bind y (fun y' -> Some (x' * y'))))
let div' x y =
  Option.(bind x (fun a ->
      bind y (fun b ->
          match b with
            0 -> none
          | _-> some (a / b))))

let main () =
  let a = None in
  let b = Some 10 in
  let funcA = fun a -> Some (10 + a) in
  let funcB a = Some (a - 12) in
  let funcC _ = None in
  (* Eftersom [Option.bind] har värde följt av funktion, så vänder vi på argumenten *)
  let bind f a = Option.bind a f in
  let aval = bind funcB (bind funcA b) in
  let bval = b |> bind funcA |> bind funcB in
  let cval = a |> bind funcA |> bind funcB in 
  let dval = b |> bind funcA |> bind funcB |> bind funcC in
  let eval = b |> bind funcA |> bind funcC |> bind funcB in
  let fval = b |> bind funcA |> bind funcA |> bind funcB in
  let option_int_to_tring = function
      Some a -> "Some "^(string_of_int a)
    | None -> "None" in
  Printf.printf "\nSkriver ut beräkningarna som gjorts\n";
  Printf.printf "aval = %s\n" (option_int_to_tring aval);
  Printf.printf "bval = %s\n" (option_int_to_tring bval);
  Printf.printf "cval = %s\n" (option_int_to_tring cval);
  Printf.printf "dval = %s\n" (option_int_to_tring dval);
  Printf.printf "eval = %s\n" (option_int_to_tring eval);
  Printf.printf "fval = %s\n" (option_int_to_tring fval)
;;
let main_b () =
  let a = None in
  let b = Some 10 in
  let funcA = fun a -> Some (10 + a) in
  let funcB a = Some (a - 12) in
  let funcC _ = None in
  (* Här definierar vi bind ( >>= ) till att vara infix [Option.bind],
     så att vi får enklare uttryck. *)
  let ( >>= ) = Option.bind in
  let aval = Option.bind (Option.bind b funcA) funcB in
  let bval = b >>= funcA >>= funcB in
  let cval = a >>= funcA >>= funcB in 
  let dval = b >>= funcA >>= funcB >>= funcC in
  let eval = b >>= funcA >>= funcC >>= funcB in
  let fval = b >>= funcA >>= funcA >>= funcB in
  let option_int_to_tring = function
      Some a -> "Some "^(string_of_int a)
    | None -> "None" in
  Printf.printf "\nSkriver ut beräkningarna som gjorts\n";
  Printf.printf "aval = %s\n" (option_int_to_tring aval);
  Printf.printf "bval = %s\n" (option_int_to_tring bval);
  Printf.printf "cval = %s\n" (option_int_to_tring cval);
  Printf.printf "dval = %s\n" (option_int_to_tring dval);
  Printf.printf "eval = %s\n" (option_int_to_tring eval);
  Printf.printf "fval = %s\n" (option_int_to_tring fval)

let () =
  main ();
  print_newline ();
  main_b ();
  print_newline ();
;;
(* eof *)
