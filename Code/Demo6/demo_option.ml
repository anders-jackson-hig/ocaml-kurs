(** Demo av 'a option
    Kompilera med C-c C-c ocamlbuild -cflag -annot demo_option.native
 *)
(*
type 'a option =
  | None
  | Some of 'a
 *)

(* Problemet är att skilja fel indata och felkod från riktiga resultat *)
let rec max_list lst =
  match lst with
  | [] -> min_int
  | head :: rst -> max head (max_list rst)

let () =
  print_int (max_list [1; 3; -1]);
  print_newline ();
  print_int (max_list []);
  print_newline ()

(* använd 'a option = None | Some v *)
let rec max_list' = function
  | [] -> None
  | head :: rst ->
     begin
       match max_list' rst with
       | None -> Some head
       | Some v -> Some (max head v)
     end

let () =
  let print_some_int a =
    begin
      match a with
      | None -> print_endline "tom"
      | Some v -> print_int v
    end;
    print_newline ()
  in
  print_some_int ( max_list' [1; -3; 3]);
  print_some_int ( max_list' [] )

let first lsd =
  match lsd with
  | [] -> failwith "Fel"
  | h :: _t -> h

let () =
  print_int (first [1; 2])
  (* print_int (first []) *)

(* Hur skriver ni [first_opt lst]?
   [ head_opt 'a list -> 'a option] *)

let first_opt lst =
  match lst with
  | [] -> Option.none      (* None *)
  | h::t -> Option.some h  (* Some h *)

let () =
  Printf.printf "\nHantera 'a option-värden\n";
  let a = first_opt [] in
  let b = first_opt [1; -3; 3] in
  (*   let c = first_opt ["a"; "b"; "c"] in *)
  Printf.printf "first_option a = %s\n" (string_of_int (Option.value a ~default:0));
  Printf.printf "first_option b = %s\n" (string_of_int (Option.value b ~default:0));
  let printint a b = Printf.printf "first_opt %s = %s\n" a (string_of_int b) in
  Option.iter (printint "1 a") a;
  Option.iter (printint "2 b") b;
  Option.iter (printint "3 None") None;
  Option.iter (printint "4 Some 2") (Some 2)

