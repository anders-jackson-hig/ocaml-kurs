(** Gör beräkningar på heltalsuttryck
    C-x C-s    : spar fil
    C-k        : rader rad/rader
    C-y        : klistrar in senast raderad rad/rader
    C-a        : början av raden 
    C-e        : slut av raden
    C-c C-c    : kompilerar fil ex med: ocambuild -cflag -annot demo_calc.byte
    M-x shell  : skapar ett skal att köra program från med ./demo_calc.byte
    C-x o      : byter fönster (ex för att köra kod)

    Till nästa föreläsning;
    Skriv om de saknade funktionerna sub''', mul''' och div'''
 *)

(** Enkel, naiv, implementation av funktioner på heltal. *)

(* Bara för att namnge signaturen hos funktionerna *)
type op1 = int -> int -> int

(* [val add: op1 *)
let add x y =
  x + y
let sub x y =
  x - y
let mul x y =
  x * y
let div x y =
  x / y

(** Testa *)

let () =
  (* [report_int] är lokal funktion i detta uttryck *)
  let report_int str v =
    (* [vstring] är lokal för [report_int] *)
    let vstring = string_of_int v in
    Printf.printf "%s = %s\n" str vstring
  in
  print_newline ();
  print_endline "### Testa add, sub, mul och div";
  print_endline "  type op1 = int -> int -> int ";
  report_int "add 11 2" (add 11 2);
  report_int "sub 11 2" (sub 11 2);
  report_int "mul 11 2" (mul 11 2);
  report_int "mul 11 0" (mul 11 0);
  print_endline "   Komposition av funktioner fungerar";
  report_int "mul (add 11 2) (sub 12 10))" (mul (add 11 2) (sub 12 10));
  report_int "div 11 2" (div 11 2);
  report_int "div (add 11 2) (sub 11 5)" (div (add 11 2) (sub 11 5));
  (* division med 0! *)
  try report_int "div 0 0" (div 11 0) with
  | Division_by_zero ->
     print_endline "Dela med 0 ger [exception Division_by_zero]!"

(** Lösning på division med noll, vi ['a option] från alla funktioner,
   och låter division med noll returnera [None], eftersom den inte har
   något värde.

   Skriv om alla funktionerna add, sub, etc till add', sub', etc. *)

type op2 = int -> int -> int option

(* [val add: op2] *)
let add' x y =
  Some (x + y)
let sub' x y =
  Some (x - y)
let mul' x y =
  Some (x * y)
(* [val div: int -> int  -> int] *)
let div' x y =
  match y with
  | 0 -> None
  | _ -> Some (x / y)

(** Testa *)

let () =
  let report_int str (v:int option) =
    let vstring =
      match v with
      | None -> "None"
      | Some value -> "Some "^(string_of_int value)
    in
    Printf.printf "%s = %s\n" str vstring
  in
  print_newline ();
  print_endline "### Testa add', sub', mul' och div'";
  print_endline "  type op2 = int -> int -> int option";
  report_int "add' 11 2" (add' 11 2);
  report_int "sub' 11 2" (sub' 11 2);
  report_int "mul' 11 2" (mul' 11 2);
  report_int "mul' 11 0" (mul' 11 0);
  report_int "div' 1 2" (div' 1 2);
  (* nu fungerar division med noll, utan exception Division_by_zero *)
  report_int "div' 11 0" (div' 11 0);
  print_endline "   MEN nu har komposition av funktioner slutat fungera!!!";
  (* Men nu kan vi inte komponera funktionerna längre, det är inte ok.
     [val mul: int -> int -> int option] 
     men funkionen [val add': int -> int -> int option] returnerar inte
     [int] längre, den returnerar [int option] *)
  print_endline " fungerar EJ: mul' (add' 11 2) (sub' 12 10))";
  print_endline " fungerar EJ: div' (add' 11 2) (sub' 11 5)"

(** Lösning är att vi låter alla funktionerna ta [int option] som
   argument.  Och om argumenten har gjort en division med noll, dvs
   värdet är [None], så returnerar vi även [None]. Vi kan ju inte göra
   något med värdet från division med noll.

   Så definiera om alla add', sub' etc till add'', sub'' etc. till att
   ta [int option -> int option -> int option] *)

(* Datatyper för alla funktioner *)
type op3 = int option -> int option -> int option

(* [val add'': op3] *)
let add'' xo yo =
  match xo with
  | None -> None (* vi har INTE hittat något användbart [xval] *)
  | Some xval -> (* vi HAR hittat ett användbart [xval] *)
     begin (* notera att [xval] finns här i denna match *)
       match yo with (* har vi något använbart [yval?] *)
       | None -> None (* nej *)
       | Some yval -> Some (xval + yval) (* ja, använd [yval] *)
     end
let sub'' xo yo =
  match xo with
  | None -> None
  | Some xval -> (* Vi har hittat ett [xval], som vi kan använda *)
     ( (* begin = ( och end = ), så koden kan skrivas så här *) 
       match yo with
       | None -> None
       | Some yval -> Some (xval - yval) (* vi har hittat användbart [yval] *)
     )
let mul'' xo yo =
  match xo with
  | None -> None
  | Some xval ->
     begin
       match yo with
       | None -> None
       | Some yval -> Some (xval * yval)
     end
let div'' xo yo =
  match xo with
  | None -> None
  | Some xval ->
     begin 
       match yo with
       | None -> None
       | Some 0 -> None (* Ingen division med 0 här inte, returnera [None] *)
       | Some yval -> Some (xval / yval)
     end

(** Testa *)

let () =
  let report_int str v =
    let vstring =
      match v with
      | None -> "None"
      | Some value -> "Some "^(string_of_int value)
    in
    Printf.printf "%s = %s\n" str vstring
  in
  print_newline ();
  print_endline "### Testa add'', sub'', mul'' och div''";
  print_endline "  type op3 = int option -> int option -> int option";
  report_int "add'' (Some 11) (Some 2)" (add'' (Some 11) (Some 2));
  report_int "add'' (Some 11) None" (add'' (Some 11) None);
  report_int "add'' None (Some 11)" (add'' None (Some 11));
  report_int "sub'' (Some 11) (Some 2)" (sub'' (Some 11) (Some 2));
  report_int "mul'' (Some 11) (Some 2)" (mul'' (Some 11) (Some 2));
  report_int "mul'' (Some 11) (Some 0)" (mul'' (Some 11) (Some 0));
  report_int "div'' (Some 11) (Some 2)" (div'' (Some 11) (Some 2));
  report_int "div'' (Some 11) (Some 0)" (div'' (Some 11) (Some 0));
  print_endline "    Och igen fungerar komposition igen";
  report_int "mul'' (add'' (Some 11) (Some 2)) (sub'' (Some 12) (Some 10)))"
    (mul'' (add'' (Some 11) (Some 2)) (sub'' (Some 12) (Some 10)));
  report_int "div'' (add'' (Some 11) (Some 2)) (sub'' (Some 11) (Some 5))"
    (div'' (add'' (Some 11) (Some 2)) (sub'' (Some 11) (Some 5)));
  (* Använd [Option.some] *)
  report_int "div'' (add'' (Option.some 11) (Option.some 2)) (sub'' (Option.some 11) (Option.some 5))"
    (div'' (add'' (Option.some 11) (Option.some 2)) (sub'' (Option.some 11) (Option.some 5)));
  (* Kan skrivas kortare, där modulen [Option] bara finns i denna [let] *)
  let open Option in
  report_int "div'' (add'' (Some 11) (Some 2)) (sub'' (Some 11) (Some 5))"
    (div'' (add'' (some 11) (some 2)) (sub'' (some 11) (some 5)))
  
  (** Men kan inte det skrivas kortare?  Jo, genom att man abstraherar
     ut att ge en variabel med [match] till en funktion.  Som dessutom
     returnerar [None] om argumenet är [None] *)

  (** Funktoinen kallas [bind], för den binder en variabel till värdet
     som [Some v] har. Så funktionen anropas med [v], om argumentet är
     [Some v].  Annars returneras [None] utan att anropa funktionen.

     [val mybind : 'a option -> ('a -> 'a option) -> 'a option] *)

let mybind v f =
  match v with
  | None -> None (* Vi har inte någon värde, returnera [None] *)
  | Some vx -> f vx (* Vi har ett värde, använd i [f vx] *)

let myreturn a =
  Some a

(* [val: add'' -> int option -> int option -> int option] *)
let add''' x y =
  mybind x (* om x har ett värde, Some v1, sätt x' till v1 *)
    (fun x' -> 
      mybind y (* om y har ett värde, Some v2, sätt y' till v2 *)
        (fun y' -> (* Vi har två värden, då köööör vi!!! *)
          myreturn (x' + y')))
(* skriv sub''' mul''' och div''' *)
let sub''' x y =
  mybind x
    (fun x' ->
      mybind y
        (fun y' ->
          myreturn (x' - y')))
let mul''' x y =
  mybind x
    (fun x' ->
      mybind y
        (fun y' ->
          myreturn (x' * y')))
let div'''  x y =
  mybind x (* om x har ett värde, Some v1, sätt x' till v1 *)
    (fun x' -> 
      mybind y (* om y har ett värde, Some v2, sätt y' till v2 *)
        (fun y' -> (* Vi har två värden, då köööör vi!!! *)
          (match y' with
           | 0 -> None
           | _ -> myreturn (x' / y'))))

(* Testa *)

let () =
  (* Lokala variabler för kortare kod *)
  let noll = Some 0 in
  let tva = Some 2 in
  let fem = Some 5 in
  let tio = Some 10 in
  let elva = Some 11 in
  let tolv = Some 12 in
  let report_int str v =
    let vstring =
      match v with
      | None -> "None"
      | Some value -> "Some "^(string_of_int value)
    in
    Printf.printf "%s = %s\n" str vstring
  in
  print_newline (); print_endline "   Detta fungerar när ni skrivit färdigt koden";
  print_endline "### Testa add''', sub''', mul''' och div'''";
  print_endline "  type op3 = int option -> int option -> int option";
  report_int "add''' elva tva" (add''' elva tva);
  report_int "add''' elva None" (add''' elva None);
  report_int "add''' None elva" (add''' None elva);
  report_int "sub''' elva tva" (sub''' elva tva);
  report_int "mul''' elva tva" (mul''' elva tva);
  report_int "mul''' elva noll" (mul''' elva noll);
  report_int "div''' elva tva" (div''' elva tva);
  report_int "div''' elva noll" (div''' elva noll);
  report_int "mul''' (add''' elva tva) (sub''' tolv tio))"
    (mul''' (add''' elva tva) (sub''' tolv tio));
  report_int "div''' (add''' elva tva) (sub''' elva fem)"
    (div''' (add''' elva tva) (sub''' elva fem));
  let open Option in (* använd [Option.some v] *)
  report_int "div''' (add''' (some 11) (some 2)) (sub''' (some 11) (some 5))"
    (div''' (add''' (some 11) (some 2)) (sub''' (some 11) (some 5)));
  print_endline "   Demonstrera mybind";
  report_int "mybind elva (fun x -> myreturn (x + 2))"
    (mybind elva (fun x -> myreturn (x + 2)));
  report_int "mybind None (fun x -> myreturn (x + 2))"
    (mybind None (fun x -> myreturn (x + 2)));
  report_int "mybind elva (fun x -> (mybind tva (fun y -> myreturn (x + y))))"
    (mybind elva
       (fun x -> (mybind tva
                    (fun y -> myreturn (x + y)))));
  report_int "mybind elva (fun x -> (mybind None (fun y -> myreturn (x + y))))"
    (mybind elva
       (fun x -> (mybind None
                    (fun y -> myreturn (x + y)))));
  (* För att få lite kompaktare kod utan så många parenteser, använde [(@@)] *)
  print_endline "   Avändandet av compostion-funktionen @@"; 
  report_int "mybind elva @@ fun x -> myreturn (x+2)"
    (mybind elva @@
       fun x -> myreturn (x+2));
  report_int "mybind elva @@ fun x -> mybind tva @@ fun y -> myreturn (x+y)"
    (mybind elva @@
       fun x -> mybind tva @@
                  fun y -> myreturn (x+y));
  report_int "mybind elva @@ fun x -> mybind None @@ fun y -> myreturn (x+y)"
    (mybind elva @@
       fun x -> mybind None @@
                  fun y -> myreturn (x+y))

(* |> pipe   val |> : v -> f -> (f v)   ((a |> b) |> c)
   @@ apply  val @@ : v -> f -> (f v)   (a @@ (b @@ c))
 *)
(** Skriv om genom att använda [Option]-modulens funktioner *)
let ( >>= ) = Option.bind  (* infix [(>>=)] som [Option.bind] *)
let return = Option.some (* [return v] "lyfter" ett värde till ['a option] *)
let add'''' x y =
  x >>= fun x' ->
  y >>= fun y' -> return (x' + y')
let div'''' x y =
  x >>= fun x' ->
  y >>= fun y' ->
  if y' = 0 then None else return (x' / y')

let _ =
  let report_int str v =
    let vstring =
      match v with
      | None -> "None"
      | Some value -> "Some "^(string_of_int value)
    in
    Printf.printf "%s = %s\n" str vstring
  in
  print_endline "Beränkna add''' 1 2";
  report_int "add'''' (return 1) (return 2)"    (add'''' (return 1) (return 2));
  report_int "return 2 |> (add'''' (return 1))" (return 2 |> add'''' (return 1));
  report_int "return 2 >>= add'''' @@ return 1" (return 2 |> add'''' @@ return 1)
  
              
(* eof *)
