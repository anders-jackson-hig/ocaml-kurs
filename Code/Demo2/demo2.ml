(* demo2.ml
 * Tips om koda i OCaml:
 *  https://ocaml.org/learn/tutorials/guidlines.html
 *)
(*
 * Emacs installation/inställningar:
 * mv .emacs .emacs.old
 * mv .emacs.d .emacs.d.ord
 *
 * git clone https://gitlab.com/anders-jackson-hig/emacs-init.git .emacs.d
 * cp .emacs.d/user-pre-settings.el.orig .emacs.d/user-pre-settings.el
 * cp .emacs.d/user-post-settings.el.orig .emacs.d/user-post-settings.el
 * emacs (svara 'no')
 * emacs .emacs.d/user-*-settings.el
 * opam install tuareg merlin
 * opam user-setup install
 * emacs &
 *)
(* Kompilera kod;
 * ocamlc -o fil fil.ml
 * ./fail
 * ocamlbuild fil.byte
 * ocamlbuild -clean
 *)
(*
 * C-x extended commands
 * C-x C-f find file
 * C-x C-s save file
 * C-x C-c exit emacs
 *
 * C-x o   other window
 * C-x 1   one window
 * C-x 2   split window horizontal
 * C-x 3   split window vertical
 *
 * OCaml i Emacs
 * C-c mode related commands
 * C-c C-c compile 
 * C-c C-x jump to error
 * C-c C-b eval buffer
 * C-c c-e eval expression
 * C-c C-t type of expression
 * C-c C-l locate definition
 * C-c Backtab expansion
 *)

(**
   Exempel på ett program

   Olika sätt att beräknar fakulteten på n:int
 *)
let rec f a =
  if a = 0 then
    1
  else
    a * f (a - 1)

let rec f1 arg =
  match arg with
    0 -> 1
  | n -> n * f1 (arg -1)

let rec f2 = fun arg -> if arg = 0 then 1 else arg * f2 (arg - 1)

let rec f3 =
  function
    0 -> 1
  | n -> n * f (n - 1)

let f4 n =
  let result = ref 1 in
  for i = 2 to n do
    result := i * !result
  done;
  !result

(**
   All kod i en fil exekveras allt eftersom den dyker upp.

   Det existerar inte någon void main() metod eller funktion.
*)
    
(* _ betyder att vi inte är intresserad av värdet *)
let _ =
  print_string "Alla uttryck utförs allt eftersom de existerar"

;;
(* här definierar vi en main funktion som bara tar () (unit) som argument *)
let main () =
  let l = Array.length Sys.argv in
  let a = if (l = 1) then begin
      int_of_string Sys.argv.(1)
    end else 10 in
  print_string "f arg = ";
  print_int (f a);
  print_newline ();
  Printf.printf "f1 %d = %d\n" a (f1 a);
  Printf.printf "f2 %d = %d\n" a (f2 a);
  Printf.printf "f3 %d = %d\n" a (f3 a);
  Printf.printf "f4 %d = %d\n" a (f4 a)
;;
(* Anropa main funktionen med unit som argument *)
main ()
    
(* eof *)
