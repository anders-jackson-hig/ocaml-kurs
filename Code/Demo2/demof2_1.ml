(** demo f2.1
    Från föreläsning/genomgång

    @author Anders Jackson (jackson@hig.se)
*)

(*
 * 15 s "teori" om Lambdakalkyl
 *
 * λx.expression
 *
 * λx.expression 12
 *
 * l = λx.expression
 *
 * l 12
 *)

(* Definiera en namnlös funktion, λ-uttryck *)
fun x -> x+1;;
(* int -> int = <fun> *)

(* Applicera en funktion på ett värde *)
(fun x -> x+1) 12;;
(* int = 13 *)
(fun x -> x+1) 13;;
(* int = 14 *)

(* λ-uttryck, funktioner, är ett värde som kan lagras i variabler *)
let a = 12
let i = fun x -> x+1;;
(* Värdet i variabeln kan sedan användas i uttryck *)
i 12;;
i a;;

(* Även i lokala variabler *)
let fi = (fun x -> x+1) in
  print_int (fi 12);;

(* print_int (fi 12);; (* Fungerar inte, eftersom fi är lokal för let-uttrycket *) *)

(print_string "Currying");;
let fxy = (fun x y -> x+y);;     (* int -> int -> int *)
let f5y = (fun x y -> x+y) 5;;   (* int -> int *)
let f54 = (fun x y -> x+y) 5 4;; (* int *)
(* eller *)
let f5y = fxy 5;;   (* int -> int *)
let f54 = f5y 4;;   (* int *)
let f59 = f5y 9;;   (* int *)

let fxy' = (fun x -> (fun y -> x+y));;
(* int -> int -> int *)

(* Datatyper *)
(* par, tupel.  Multiplikativ datatyp *)
(1, "a") ;;
(* int * string *)

(* lista *)
[1; 2; 3];;
(* int list *)
[];;
(* 'a list ("alfa list" eller "tick a list") *)

(* array *)
[| 1; 2; 3 |];;
(* int array *)
[| 1; 2; 3 |].(1) = 2;; (* väljer element 1 ur array *)

(* variants, "enum".  Additativ datatyp *)
type month  = Jan | Feb | Mars | April | Maj | Juni | Juli | Aug | Sept | Okt | Nov | Dec ;;
let m:month = Maj ;;  (* Maj är en konstruktor, som skapar ett data för datatypen month *)

let month_of_string =
  fun (s:string) ->
    if s = "Jan" then Jan
    else if s = "Feb" then Feb
    else Dec;;
month_of_string "Jan";;
month_of_string "Feb";;
month_of_string "kaka";;

type kaka = Int of int | Float of float | Boolean;;
let v1:kaka = Int 12;;
let v2:kaka = Float 12.;;
let v3:kaka = Boolean;;

(* Funktioner *)
fun x y -> x+y;;

let f = fun x y -> x+y;;
let f' x y = x+y;;  (* syntaktiskt socker, praktiskt sätt att skriva samma sak på *)
(* let f'' = function x -> (function y -> x + y);; *)

(** 2. fakultet-funktioner *)  (* detta är en OCaml-doc, ungefär som Java-doc *)

(** [fac a] beräknar fakulteten rekursivt med ett [if]-uttryck, dvs a!
   (detta är Ocaml-doc) *)
let rec fac a =
  if a = 0 then
    1
  else
    a * fac (a-1)

(** [fac' a] beräknar fakuteten rekursivt med hjälp av [match] *)
let rec fac' a =
  match a with
  | 0 -> 1
  | n -> n * fac' (n-1)
(* Notera att vi skapar en nu variabel vid [match].  Så [match] är en
   case på stereoider.  Notera att första | inte strikt behövs. Men
   det får skrivas dit. *)

(** [fac'' a] beräknar fakulteten rekursivt, med hjälp av [function] *)
let rec fac'' =
  function
  | 0 -> 1
  | n -> n * fac'' (n-1)
(* function tar alltid ett argument, som den implicit gör en match av.
   Ser ni likheterna mellan [fun] och [function]?  fun x -> x och
   function x -> x (eller function | x -> x) *)

(** [fac''' n] beräknar fakulteten med en [for]-sats. I den här kursen
   så undviker vi att använda [for] och [while]. *)
let fac''' n =
  let result = ref 1 in  (* variabler kan inte ändra, men [ref]-variabler kan. Undvik! *)
  for i = 1 to n do      (* [for]-sats, som låter i få från 1 till n mellan [do] och [done] *)
    result := i * !result (* [:=] tilldelar en [ref] nytt värde,  [!] före hämtar dess värde *)
  done;
  !result ;; (* returnera värdet av [ref]-variabeln *)
;;

(* Hur man gör en OCaml-fil till ett körbart program *)

(* Program i OCaml har ingen main-metod som Java eller main()-funktion
   som i C och C++.  Ett OCaml-program utför alla uttryck allt
   eftersom programmet/filen gås igenom.

   Men man brukar göra en fuktion som heter main, och som anropas sist
   i "huvud"-filen som startar resten av programmet.

   Så här kommer jag att skriva en main()-funktion som tittar på
   argumentet som man startar programmet med.  Om programmet inte har
   ett argument när det startas, så kommer programmet att anta ett
   värde.  Detta värde kommer sedan att användas när de ovan
   definierade fakultetsfunktionerna anropas och resultaten skrivs
   ut. *)

(* Variabler som inleds med _ kommer i OCaml att ignoreras. Det är en
   markering att variabeln inte är intressant. Notera även att det
   normala är att exekvera uttryck med hjälp av ett let-uttryck som
   här.
*)
let _ =
  print_newline ();
  print_endline "Hej, detta kommer att skrivas ut när programmet körs!" ;;

(* Här kommer main-funktionen.

   Den tar längden på arrayen argument till programmet. Är antalet
   argument, dvs längden på arrayen Sys.argv, två så kommer det andra
   argumentet att omvandlas till heltal och användas vid anropen av
   fakultetfunktionerna. Första elementet i Sys.argv är programmets
   namn.

   Notera att begin .. end fungerar precis som ( .. ), och används för
   att gruppera uttryck som är i sekvens.  Det går lika bra att
   använda ( ... ), men blir tydligare med begin .. end.

   Utskrifterna av resultatet använder vi först print_newline,
   print_string och print_int.  Dessa blir lite bökigare, så sedan
   anävnder vi istället funktionen printf från modulen Printf.  Den
   tar först en fotmatsträng på liknande format som format/printf i
   Java och printf i C/C++.

   Notera att main bara tillåter att vi anropar den med unit.

   Prova gärna att skriva ut värdet av Sys.argv i utop/ocaml REPL.
*)
let main () =
  let len = Array.length Sys.argv in
  let a = if (len = 2) then begin
      int_of_string Sys.argv.(1)
    end else
      10 in
  print_newline ();
  print_string "fac a = ";
  print_int (fac a);
  print_newline ();
  Printf.printf "fac %d = %d\n" a (fac a);
  Printf.printf "fac' %d = %d\n" a (fac' a);
  Printf.printf "fac'' %d = %d\n" a (fac'' a);
  Printf.printf "fac''' %d = %d\n" a (fac''' a);
  for i = 0 to a do
    Printf.printf "fac %2d = %d\n" (a-i) (fac (a-i));
  done
;;

(** Anropa [main ()] när programmet startar *)

let _ =
  main ()

(* Kompilera filen med:
   $ ocamlbuild demof2_1.native 
   eller
   $ ocamlbuild demof2_1.bytes
   
   Från Emacs:
   M-x shell
   eller
   C-c C-c och skriv in kommando ovan.

   Kör kommandot i skalet (M-x shell)
   Notera att det går att trycka på uppåtpilen när 
   Emacs frågar efter kommando.
*)

(* eof *)
