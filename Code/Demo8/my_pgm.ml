(* my_pgm.ml *)

(** [My_pgm] är huvudprogrammet som vi skall göra med hjälp av
   mönstret Trainstaion.

    Programmet skall läsa in två tal från kommandoraden och addera
   dem.

    Om växeln [-s] eller [--subtract] används, så skall istället talen
    subtraheras. *)

(* Programmet består av filerna
   - [my_pgm.ml] :: implementerar huvudprogrammet
   - [myargs.ml]/[myargs.mli] :: implementerar tolkning av
     kommandoraden samt hanterar tillståndet för dem till de andra
     delarna i [Myargs.state].
   - [calc.ml]/[calc.mli] :: implementerar beräkningen, beroende av
     tillståndet.
   - [myprint.ml]/[myprint.mli] :: implementerar utskrift av
     resultatet, beroende på tillståndet.
*)

let _ =
  Myargs.parse Sys.argv |> Calc.calculate |> Myprint.output_result

(* eof *)
