(** Huvudprogrammet för [kbnum].

   Programmet startar anropar modulerna i [Kbnum], som finns under
   [./lib]

 *)

(* Tolka alla argumenten först i programmet *)
let () =
  print_endline "Tolka argumenten med Arg"

(*
let () =
  Kbnum.Databas.BuildDb.create_db ()
 *)

(* Starta programmet *)
(* [Fun.flip] byter ordning på argumenten i funktionen *)
let () =
  print_endline "Testing";
  let open Kbnum.Status in
  empty |>
    (Fun.flip Result.bind) (set_filnamn "hello") |>
    Result.map (fun d -> Option.iter print_endline (get_filnamn d)) |>
    ignore

(* eof *)
