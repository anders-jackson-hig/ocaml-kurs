(** [databas] hanterar läsning och skrivning till [sqlite3].
    Vi använder oss av [sqlite3] och hanterar [movie_record] för att
    hantera in och ut-matning *)

type movie_record = {
    title : string;
    cast : string list;
    year : int;
  } [@@deriving yojson {strict = false}]

(* skapar to_yojson on from_yojson *)



module CleanName = struct
  let r1 = Str.regexp {|^\[\[\(.*\)\]\]|}
  let r2 = Str.regexp {|\(|.*\)$|}
  let r3 = Str.regexp {| *(.*actor) *$|}
  let r4 = Str.regexp {|'|}

  let clean_name s =
    Str.replace_first r1 "\\1" s |>
      Str.replace_first r2 "" |>
      Str.replace_first r3 ""
end

let clean_name = CleanName.clean_name

module type DBX = sig val mydb: Sqlite3.db end

let opendb ?(fn="kbnum.db")  () =
  (module struct let mydb = Sqlite3.db_open fn end:DBX)

module DBI (X:DBX) = struct

  include X

  let dberr rc  =
    prerr_endline (Sqlite3.Rc.to_string rc);
    prerr_endline (Sqlite3.errmsg mydb)

  (** Execute sql statement *)
  let exe sql =
    let open Sqlite3 in
    match exec mydb sql with
    | Rc.OK -> ()
    | rc -> dberr rc

  let prepare sql = Sqlite3.prepare mydb sql

  type data =
    | Integer of int
    | String of string

  let bindlist stmt data =
    ignore @@ Sqlite3.reset stmt;
    ignore @@
    List.fold_left (fun pos datum ->
        ignore (match datum with
        | Integer x -> Sqlite3.bind_int stmt pos x
        | String  x -> Sqlite3.bind_text stmt pos x); pos+1) 1 data

  let get stmt =
    let open Sqlite3 in
    match step stmt with
    | Rc.ROW ->
       begin
         match (row_data stmt).(0) with
         | Data.TEXT s -> Some (String s)
         | Data.INT  x -> Some(Integer (Int64.to_int x))
         | _ -> None
       end
    | _ -> None

end


module BuildDb(X:DBX) = struct

  include DBI(X)

  let create_db () =
    let sql =
      {|
       DROP TABLE IF EXISTS actors;
       CREATE TABLE actors (
          actor_id INTEGER PRIMARY KEY,
          actor_name TEXT NOT NULL
       );
       CREATE UNIQUE INDEX actor_name_idx ON actors (actor_name);
       DROP TABLE IF EXISTS movies;
       CREATE TABLE movies (
          movie_id INTEGER PRIMARY KEY,
          movie_name TEXT NOT NULL
       );
       CREATE UNIQUE INDEX movie_name_idx ON movies (movie_name);
       DROP TABLE IF EXISTS parts;
       CREATE TABLE parts (
          movie_id INTEGER NOT NULL,
          actor_id INTEGER NOT NULL,
          PRIMARY KEY (movie_id, actor_id)
       );
       CREATE INDEX parts_actor_idx ON parts (actor_id);
       |}
    in
    exe sql

  let _ = create_db ()

  let begin_transaction = exe "BEGIN TRANSACTION; "
  let end_transaction = exe "END TRANSACTION; "

  type id_tbl = {
    tbl   : (string, int) Hashtbl.t;
    count : int ref;
    stmt  : Sqlite3.stmt
  }

  (** Prepared statements *)
  let insert_actor = prepare "INSERT INTO actors VALUES(?,?);"
  let insert_movie = prepare "INSERT INTO movies VALUES(?,?);"
  let insert_parts = prepare "INSERT INTO parts  VALUES(?,?);"

  let create_hash_tbl stmt = {
    tbl = Hashtbl.create 100000;
    count = ref 10000;
    stmt = stmt;
  }

  let actors = create_hash_tbl insert_actor
  let movies = create_hash_tbl insert_movie

  let insert_is stmt i str =
    let open Sqlite3 in
    ignore @@ reset stmt;
    ignore @@ bind_int stmt 1 i;
    ignore @@ bind_text stmt 2 str;
    match step stmt with
    | Rc.OK | Rc.DONE -> Some (i, str)
    | rc -> dberr rc; None

  let insert_ii stmt i1 i2 =
    let open Sqlite3 in
    ignore @@ reset stmt;
    ignore @@ bind_int stmt 1 i1;
    ignore @@ bind_int stmt 2 i2;
    match step stmt with
    | Rc.OK | Rc.DONE -> Some (i1, i2)
    | rc -> dberr rc; None

  let get { tbl; count; stmt } name =
    match Hashtbl.find_opt tbl name with
    | Some id -> Some id
    | None ->
      incr count;
      Hashtbl.add tbl name (!count);
      match insert_is stmt (!count) name with
      | None -> None
      | Some (id, _name) -> Some id

  let tabulate title cast =
    match get movies (clean_name title) with
    | None -> 0
    | Some movie_id ->
      let rec aux acc = function
        | [] -> acc
        | name :: cast ->
          begin
            match get actors name with
            | None -> aux acc cast
            | Some actor_id ->
              match insert_ii insert_parts movie_id actor_id with
              | Some _ -> aux (acc + 1) cast
              | None ->
                prerr_endline ("Movie: " ^ title ^ " Actor: " ^ name);
                aux acc cast
          end
      in
      aux 0 (List.map clean_name cast)

  let read_file_into_db filename =
    begin_transaction;
    let chan = open_in filename in
    let count = ref 0 in
    try
      while true do
        let line = input_line chan in
        match Yojson.Safe.from_string line |>
              movie_record_of_yojson with
        | Result.Ok { title ; cast; _ } ->
          count := !count + (tabulate title cast)
        | Result.Error _ -> ()
      done;
      !count
    with End_of_file ->
      close_in chan;
      end_transaction;
      ignore @@ Sqlite3.db_close mydb;
      !count
end

module type KB = sig
  (** Hörntyper *)
  type typ = Actor | Movie
  type id = int
  type vertex = typ * id (* Hörn i grafen *)

  (** [to_name v] ger namn på motsvarande film/skådis. [None] om ingen
      koppling finns *)
  val to_name: vertex -> (typ*string) option
  (** [of_name typ v] ger id på motsvarande film/skådis. [None] om namnet inte
      giltigt. *)
  val of_name: (typ*string) -> vertex option

  (** [fold_neighbours f v acc] applicerar [f w acc] över alla grannar [w] till [v] i
      kb-grafen *)
  val fold_neighbours: (vertex -> 'a -> 'a) -> vertex -> 'a -> 'a

  val fold_actors: (vertex*string -> 'a -> 'a) -> unit -> 'a -> 'a
  val fold_movies: (vertex*string -> 'a -> 'a) -> unit -> 'a -> 'a
end

module KB(X:DBX) : KB = struct

  include DBI(X)

    (** Hörntyper *)
    type typ = Actor | Movie
    type id = int
    type vertex = typ * id (* Hörn i grafen *)

  let actor x = Actor,x
  let movie x = Movie,x

  let get_actor_name_stmt   = prepare "SELECT actor_name FROM actors WHERE actor_id = ?;"
  let get_movie_name_stmt   = prepare "SELECT movie_name FROM movies WHERE movie_id = ?;"
  let get_actor_id_stmt     = prepare "SELECT actor_id   FROM actors WHERE actor_name = ?;"
  let get_movie_id_stmt     = prepare "SELECT movie_id   FROM movies WHERE movie_name = ?;"
  let get_actor_part_stmt   = prepare "SELECT actor_id   FROM parts  WHERE movie_id = ?;"
  let get_movie_part_stmt   = prepare "SELECT movie_id   FROM parts  WHERE actor_id = ?;"

  let bindi stmt i =
    ignore @@ Sqlite3.reset stmt;
    Sqlite3.bind stmt 1 (Sqlite3.Data.INT (Int64.of_int i))

  let binds stmt s =
    ignore @@ Sqlite3.reset stmt;
    Sqlite3.bind stmt 1 (Sqlite3.Data.TEXT s)

  let to_name (typ,x) =
    let stmt =
      match typ with
      | Actor -> get_actor_name_stmt
      | Movie -> get_movie_name_stmt
    in
    ignore @@ bindi stmt x;
    match get stmt with
    | Some(String name) -> Some (typ,name)
    | _ -> None

  let of_name (typ,x) =
    let stmt =
      match typ with
      | Actor -> get_actor_id_stmt
      | Movie -> get_movie_id_stmt
    in
    ignore @@ binds stmt x;
    match get stmt with
    | Some(Integer x) -> Some (typ,x)
    | _ -> None

  let fold_neighbours f (typ,x) acc =
    let open Sqlite3 in
    let (ntyp,stmt) =
      match typ with
      | Actor -> Movie,get_movie_part_stmt
      | Movie -> Actor,get_actor_part_stmt
    in
    let ff acc row = match row.(0) with
      | Data.INT i64 ->
         let i = Int64.to_int i64 in
         f (ntyp,i) acc
      | _ -> acc
    in
    ignore @@ bindi stmt x;
    let (rc, acc') = Sqlite3.fold stmt ~f:ff ~init:acc in
    match rc with
    | Rc.OK | Rc.DONE -> acc'
    | rc -> dberr rc; acc'

  let fold_actors f () acc =
    let sql = "select actor_id, actor_name from actors;" in
    let acc' = ref acc in
    let cb row =
      match (row.(0),row.(1)) with
      | Some ids, Some name -> acc' := f ((Actor,int_of_string ids), name) (!acc')
      | _ -> ()
    in
    ignore @@ Sqlite3.exec_no_headers mydb ~cb sql;
    !acc'

  let fold_movies f () acc =
    let sql = "select movie_id, movie_name from movies;" in
    let acc' = ref acc in
    let cb row =
      match (row.(0),row.(1)) with
      | Some ids, Some name -> acc' := f ((Movie,int_of_string ids), name) (!acc')
      | _ -> ()
    in
    ignore @@ Sqlite3.exec_no_headers mydb ~cb sql;
    !acc'

end
