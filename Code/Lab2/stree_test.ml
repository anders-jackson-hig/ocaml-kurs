(* file: stree_test.ml *)
(* Filer:

   - .merlin :: För att Merlin skall fungera (Exempelvis: C-c C-t)
   - Makefile :: För att kompilering skall fungera med [make -k]
                 Prova [make clean] och [make -k all]
   - stree_test.ml :: Ett testprogram [Stree_test] för [Stree]
   - stree.ml :: Själva implementeringen av Sorted Binary Tree.

   Ladda denna fil i emacs. C-c C-c "make -k"
   M-x shell (Alt-x shell). "./stree_test.byte". Klart
*)

let main () =
  let module StringTree = Stree.Make(String) in
  let module StrangePari = struct
    type t=int*int
    let compare a b = ((fst a) - (snd b) + (snd a) - (fst b))
  end in
  let module PariTree = Stree.Make(StrangePari) in
  let a = StringTree.(empty
                      |> add ("a",12)
                      |> add ("b",(-32))) in
  let b = StringTree.(empty
                      |> add ("a", (1, 43.3))
                      |> add ("b", (2, 32.))) in
  let c = PariTree.(empty
                    |> add ((1,2), "a")
                    |> add ((2,1), "b")
                    |> add ((2,1), "c")) in
  print_endline "Testing Stree:";
  print_endline "Cardinal of a, b and c";
  print_endline (string_of_int (StringTree.cardinal a));
  print_endline (string_of_int (StringTree.cardinal b));
  print_endline (string_of_int (PariTree.cardinal c));

  print_endline "keys and values for a, b and c";
  List.iter (fun s -> Printf.printf "%s " s) (StringTree.keys a); print_newline ();
  List.iter (fun n -> Printf.printf "%d " n) (StringTree.values a); print_newline ();

  List.iter (fun s -> Printf.printf "%s " s) (StringTree.keys b); print_newline ();
  List.iter (fun (n1,n2) -> Printf.printf "(%d %f) " n1 n2) (StringTree.values b); print_newline ();

  List.iter (fun (n1,n2) -> Printf.printf "(%d,%d) " n1 n2) (PariTree.keys c); print_newline ();
  List.iter (fun s -> Printf.printf "%s " s) (PariTree.values c); print_newline ();

  print_endline "cardinal after remove key in a";
  print_int (StringTree.cardinal (StringTree.remove "a" a)); print_newline ();
  print_int (StringTree.cardinal (StringTree.remove "b" a)); print_newline ()

let () =
  main ()

 (* eof *)
