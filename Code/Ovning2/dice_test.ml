module SixDie = Die.Make(struct let sides=6 end)

module TwentyDie = Die.Make(struct let sides=20 end)

;;
let main () =
  Printf.printf "Testning av Die:\n";
  SixDie.(throws new_die |> eval |> Printf.printf "D%d: %d\n" sides);
  SixDie.(throws new_die |> throws |> eval |> Printf.printf "D%d: %d\n" sides);
  TwentyDie.(throws new_die |> eval |> Printf.printf "D%d: %d\n" sides);
  TwentyDie.(throws new_die |> throws |> eval |> Printf.printf "D%d: %d\n" sides);
  Printf.printf "Nästa del:\n";
  let p_print d = TwentyDie.eval d |> print_int |> print_newline in
  let t1 = TwentyDie.throws TwentyDie.new_die in
  let t2 = TwentyDie.throws t1 in
  begin
    p_print t1;
    p_print t2
  end
;;
(* starta programmet *)
let () =
  main ()
