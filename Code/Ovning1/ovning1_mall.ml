(*
 * Läs argument och växlar från kommandoraden.
 *
 * https://ocaml.org/learn/tutorials/command-line_arguments.html
 * http://caml.inria.fr/pub/docs/manual-ocaml/libref/Arg.html
 *
 * Här använder vi modulen Arg från standardbiblioteket.
 * 
 * Progammet specificerar några växlar, och tar och sedan 
 * tolkar och sätter några variabler utifrån vad som kommer på
 * kommandoraden. 
 *
 * ocamlbuild -cflags -annot ovning1.byte
 * 
 * $ ./ovning1.byte skapa1
 * $ -/ovning1.byte --help
 *
 *)

(* Skapa variablerna som sätts enligt kommandoraden *)
let skapa1a = ref false
let skapa2a = ref false
let skapa3a = ref false
let restofargs = ref ""

(* Tolka kommandoraden *)
let () =
  let spec = [("-skapa1", Arg.Set skapa1a, "\t testar skapa1");
              ("-skapa2", Arg.Set skapa2a, "\t testar skapa2");
              ("-skapa3", Arg.Set skapa3a, "\t testar skapa3");
              ("--", Arg.Rest (fun s -> restofargs := !restofargs ^ s ^ " "),
               "\t alla övriga argument");]
  in
  Arg.parse
    (Arg.align spec)
    (fun str -> restofargs := ! restofargs ^ str ^ " ")
    "Implementerar övning1.\n"

let skapa1 a b c =
  []

let () =
  let skrivut_skapa1 lista =
    print_endline "skapa1"
  in
  (* Ok, så nu skriver vi ut argumenten. *)
  if !skapa1a then
    skrivut_skapa1 (skapa1 4 10 2)
  ;
  Printf.(printf "skapa1:            %b\n" !skapa1a;
          printf "skapa2:            %b\n" !skapa2a;
          printf "skapa3:            %b\n" !skapa3a;
          printf "Rest of line: %s\n" !restofargs)

(* eof *)
    
    
